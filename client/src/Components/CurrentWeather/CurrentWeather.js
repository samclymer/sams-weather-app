import React from "react";
import "./CurrentWeather.css";
import CurrentBlock from "../CurrentBlock/CurrentBlock";

class CurrentWeather extends React.Component {
  /*constructor() {
    super();
  }*/

  render() {
    return (
      <div>
        {this.props.currentResults.map(next => {
          return <CurrentBlock key={next.id} next={next} />;
        })}
      </div>
    );
  }
}

export default CurrentWeather;
