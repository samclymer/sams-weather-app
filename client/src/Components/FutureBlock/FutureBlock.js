import React from "react";
import "./FutureBlock.css";

class FutureBlock extends React.Component {
  /*constructor() {
    super();
  }*/

  render() {
    return (
      <tr>
        <td className="align-middle">{this.props.next.futureDate}</td>
        <td className="align-middle">
          <img
            src={require(`../../graphics/${this.props.next.futureIcon}.png`)}
            className="icon-size"
            alt=""
          />
        </td>
        <td className="align-middle">{this.props.next.futureDescription}</td>
        <td className="align-middle">
          {Math.round(this.props.next.futureLowTemp)}&deg;/{Math.round(this.props.next.futureHighTemp)}&deg;
        </td>
        <td className="align-middle">{Math.round(this.props.next.futureUVIndex)}</td>
        <td className="align-middle">{this.props.next.futureHumidity}%</td>
        <td className="align-middle">
          {Math.round(this.props.next.futureWindSpeed)} mph {this.props.next.futureWindDirection}
        </td>
      </tr>
    );
  }
}

export default FutureBlock;
