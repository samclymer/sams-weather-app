import React from "react";
import "./FutureWeatherLoading.css";

class FutureWeatherLoading extends React.Component {
    /*constructor() {
      super();
    }*/

    render() {
        return (
            <div class="container">
                <div class="d-flex justify-content-center">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default FutureWeatherLoading;