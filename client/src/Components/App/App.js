import React from "react";
import "./App.css";
import SearchBar from "../SearchBar/SearchBar";
import CurrentWeather from "../CurrentWeather/CurrentWeather";
import FutureWeather from "../FutureWeather/FutureWeather";
import CurrentWeatherLoading from "../CurrentWeatherLoading/CurrentWeatherLoading";
import FutureWeatherLoading from "../FutureWeatherLoading/FutureWeatherLoading";
import WeatherBit from "../../utility/WeatherBit";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentResults: [],
      futureResults: [],
      loadingCurrent: false,
      loadingFuture: false,
      renderFuture: false
    };
    this.search = this.search.bind(this);
  }

  search(city, state) {
    this.setState({ loadingCurrent: true });
    WeatherBit.currentSearch(city, state)
      .then(currentResults => {
        this.setState({ currentResults: currentResults, loadingCurrent: false });
      })
      .then(
        this.setState({ loadingFuture: true }),
        WeatherBit.futureSearch(city, state).then(futureResults => {
          this.setState({ futureResults: futureResults, renderFuture: true, loadingFuture: false });
        })
      );
    console.log("search completed");
  }

  render() {
    const loadingCurrent = this.state.loadingCurrent;
    const loadingFuture = this.state.loadingFuture;
    const renderFuture = this.state.renderFuture;
    return (
      <div>
        <div className="container header-main p-4">
          <div className="row">
            <div className="col-lg-5 p-2">
              <img
                alt=""
                src={require("../../graphics/Sams-Weather-App.png")}
              />
            </div>
            <div className="col-lg-7 p-2">
              <p className="lead">
                Enter City and State in the fields below to recieve both a current and 16-day forecast for that location.
              </p>
              <hr className="my-4 hr-header" />
              <SearchBar onSearch={this.search} />
            </div>
          </div>
        </div>
        <div className="container">
          {loadingCurrent === true ? <CurrentWeatherLoading /> : <CurrentWeather currentResults={this.state.currentResults} />}
          <div className="row"></div>
          <div className="row">
            <div className="col-lg-12 p-1 mt-1">
              {renderFuture === true ? <FutureWeather futureResults={this.state.futureResults} /> : null}
              {loadingFuture === true ? <FutureWeatherLoading /> : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
