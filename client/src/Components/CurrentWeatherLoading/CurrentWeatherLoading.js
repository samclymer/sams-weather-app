import React from "react";
import "./CurrentWeatherLoading.css";


class CurrentWeatherLoading extends React.Component {
    /*constructor() {
      super();
    }*/

    render() {
        return (
            <div class="container">
                <div class="d-flex justify-content-center">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default CurrentWeatherLoading;