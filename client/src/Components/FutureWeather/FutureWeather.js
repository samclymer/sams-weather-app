import React from "react";
import "./FutureWeather.css";
import FutureBlock from "../FutureBlock/FutureBlock";

class FutureWeather extends React.Component {
    /*constructor() {
    super();
  }*/

    render() {
        return (
            <div className="table-responsive">
            <table className="table table-borderless table-font text-center">
              <thead>
                <tr>
                  <th className="align-middle">Date</th>
                  <th className="align-middle"> </th>
                  <th className="align-middle">Description</th>
                  <th className="align-middle">Temperature (Lo/Hi)</th>
                  <th className="align-middle">UV Index</th>
                  <th className="align-middle">Humidity</th>
                  <th className="align-middle">Wind</th>
                </tr>
              </thead>
              <tbody>
              {
         this.props.futureResults.map((next) => {
           return <FutureBlock key={next.id} next={next} />
         })
       }
              </tbody>
            </table>
          </div>
        );
      }
}

export default FutureWeather;