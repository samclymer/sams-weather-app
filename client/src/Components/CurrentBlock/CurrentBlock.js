import React from "react";
import "./CurrentBlock.css";

class CurrentBlock extends React.Component {
  /*constructor() {
    super();
  }*/

  render() {
    return (
        <div>
        <div className="row mb-3">
          <div className="col-sm-12 city-div">
            <h3 className="lead city-text">{this.props.next.currentCity}, {this.props.next.currentStateCode}</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 left-div">
            <h1 className="left-font-primary">{Math.round(this.props.next.currentTemp)} &deg;</h1>
            <img
            className="icon-position p-3 icon-size"
            src={require(`../../graphics/${this.props.next.currentIcon}.png`)}
            alt=""
          />
            <h3 className="lead left-font-secondary">{this.props.next.currentDescription}</h3>
          </div>
          <div className="col-sm-6 right-div">
            <h3 className="lead right-font-secondary">Feels Like: {Math.round(this.props.next.currentFeelsLike)} &deg;</h3>
            <hr className="my-3" />
            <h3 className="lead right-font-secondary">Wind: {Math.round(this.props.next.currentWindSpeed)} mph {this.props.next.currentWindDirection}</h3>
            <hr className="my-3" />
            <h3 className="lead right-font-secondary">Humidity: {this.props.next.CurrentHumidity}%</h3>
            <hr className="my-3" />
            <h3 className="lead right-font-secondary">UV Index: {Math.round(this.props.next.currentUVIndex)}</h3>
          </div>
        </div>
      </div>
    );
  }
}

export default CurrentBlock;