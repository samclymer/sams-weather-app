import Key from "../config/default";

const key = Key;

const WeatherBit = {
  currentSearch(city, state) {
    console.log(city, state);
    return fetch(
      `https://api.weatherbit.io/v2.0/current?city=${city},${state}&key=${key}&units=I`,
      {
        method: "GET",
        headers: {
          "Access-Control-Allow-Origin": "*"
      }
      }
    )
      .then(response => {
        console.log(response);
        if (!response.ok || response.statusText === "No Content") {
          alert(
            "Location/Forecast could not be found - Please try again"
          );
          document.location.reload(true);
          /*throw Error(response.statusText);*/
        }
        return response.json();
      })
      .then(data => {
        if (!data.data) {
          return alert(
            "Location/Forecast could not be found - Please try again"
          );
        }
        console.log(data.data);
        return data.data
          .map(current => ({
            currentID: current.id,
            currentCity: current.city_name,
            currentStateCode: current.state_code,
            currentTemp: current.temp,
            currentDescription: current.weather.description,
            currentFeelsLike: current.app_temp,
            currentWindDirection: current.wind_cdir,
            currentWindSpeed: current.wind_spd,
            CurrentHumidity: current.rh,
            currentUVIndex: current.uv,
            currentIcon: current.weather.icon
          }))
          /*.catch(error => {
            console.error("Looks like there was a problem: \n", error);
          });*/
      });
  },

  futureSearch(city, state) {
    console.log(city, state);
    return fetch(
      `https://api.weatherbit.io/v2.0/forecast/daily?city=${city},${state}&key=${key}&units=I`,
      {
        method: "GET",
        headers: {
          "Access-Control-Allow-Origin": "*"
      }
      }
    )
      .then(response => {
        console.log(response);
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(data => {
        if (!data.data) {
          return alert(
            "Future forecast could not be obtained - Please try again"
          );
        }
        console.log(data.data);
        return data.data
          .map(future => ({
            futureID: future.id,
            futureDate: future.valid_date,
            futureLowTemp: future.min_temp,
            futureHighTemp: future.max_temp,
            futureDescription: future.weather.description,
            futureWindDirection: future.wind_cdir,
            futureWindSpeed: future.wind_spd,
            futureHumidity: future.rh,
            futureUVIndex: future.uv,
            futureIcon: future.weather.icon
          }))
          /*.catch(error => {
            console.error("Looks like there was a problem: \n", error);
          });*/
      });
      
  }
};


export default WeatherBit;

//https://api.weatherbit.io/v2.0/current?city=${term}&key=${key}&units=I
